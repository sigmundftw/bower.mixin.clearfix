# SASS mixin clearfix

## Objectif

Cette mixin permet à une balise d'avoir une hauteur correct même si celui-ci
contient des balises utilisant le float.

Cette mixin est compatible avec IE7 et +, ainsi que Chrome, Firefox et Safari.

## Installation

Inclure la mixin SASS ```dist/bower.mixin.clearfix.sass``` au projet SASS, et utiliser la 
mixin ```clearfix``` pour générer les sélecteurs.


**Ex :**  
``` 
.classe { @include clearfix; }
```

## ChangeLog

**v1.0.0**  
Installation de la version initial de la mixin existante.